##
# Word mixing methods
# @author Wade Harkins <wade@ventureinteractive.com>
#
class WordMix

  VOWELS = /[aeiouAEIOUyY]/

  def self.divide(string)
    letters = []
    for i in 0..string.length
      letters << string[i]
    end
    return letters.compact
  end

  def self.mark_vowels(letters,marker='!')
    letters = divide(letters) if !letters.is_a?(Array)
    letters.map { |l| (!l.match(WordMix::VOWELS).nil?)? "#{l}#{marker}" : l }
  end

  ##
  # Divide a word into an array, breaking apart on vowels
  # @param [String, Array] letters Word to divide
  # @param [String] marker Marker symbol used with mark_vowels
  # @return [Array] Array of word parts broken up on vowels
  #
  def self.divide_by_vowels(letters,marker='!')
    mark_vowels(letters,marker).join("").split(marker)
  end


  ##
  # Divide and mix two or more words together
  # @param [Array] words Array of strings or arrays of words
  # @param [Hash] options Optional parameter hash
  # @option options [Boolean] :remove_dupes Remove duplicate particles 
  #   (default is false)
  # @option options [Integer] :max_parts Limit maximum number of parts used 
  #   in permutation (default is to use all particles)
  # @return [Array] Array of resulting permutations
  #
  def self.divide_and_mix(words=[],options={})
    remove_dupes = options.fetch(:remove_dupes, false)
    max_parts = options.fetch(:max_parts, 65535)

    divided_parts = words.map{ |w| divide_by_vowels(w) }.flatten

    divided_parts = divided_parts.uniq if remove_dupes

    mix = divided_parts.permutation([divided_parts.length, max_parts].min).to_a

    mix.map{|p| p.join("") }
  end



  ##
  # Mix word by individual letters or vowel delimited particles
  # @param [String] word Word to process
  # @param [Hash] options Options parameter hash
  # @option options [Boolean] :vowels Process vowel-delimited chunks 
  #   (default is false)
  # @option options [Integer] :seed Optional random seed for mixing, ignored
  #   if 0 (default is 0)
  # @option options [Boolean] :skip_join Don't rejoin particles before returning
  #   (default is false)
  # @return [String, Array] Mixed String or Array, depending on value of :skip_join
  #
  def self.mix(word, options={})
    vowels = options.fetch(:vowels, false)
    seed = options.fetch(:seed, 0)
    rejoin = !options.fetch(:skip_join, false)
    # Convert word to an array if it isn't already
    if !word.is_a?(Array)
      # Divide per letter if vowels is false
      word = divide(word) unless vowels
      # Divide by vowels if vowels is true
      word = divide_by_vowels(word) if vowels
    end
    word.shuffle!( random: Random.new(seed) ) unless seed == 0
    word.shuffle! if seed == 0

    word.join("") if rejoin

  end

  def self.mutate(base_word, addon,levels=1)
    base_letters = WordMix::divide(base_word)
    add_letters = WordMix::divide(addon)

    base_parts = mark_vowels(base_letters).join("").split("!")
    add_parts = mark_vowels(add_letters).join("").split("!")

    base, add = [], []

    base[0] = base_parts.permutation(base_parts.length).to_a
    add[0] = add_parts.permutation(add_parts.length).to_a

    for i in 0..levels do
      base[i+1] = mutant_mix(base,add,i)
    end

    return base


  end

  def self.mutant_mix(base,addition,src_level=0)
    result = []
    base[src_level].each do |b|
      # Only process if there is addition at source level
      if !addition[src_level].nil?
        a = ([b] + addition[src_level]).flatten.compact
        am = a.permutation(a.length).to_a.compact
        result << [am.compact]
      else
        a = -1
      end
    end
    result.flatten
  end

end