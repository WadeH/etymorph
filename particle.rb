##
# Word particle array modification methods
# @author Wade Harkins <wade@ventureinteractive.com>
# 
class Particle

  ##
  # Create array of particles from a word, either per letter or vowel delimited
  # @param [String] word Word to particleize
  # @param [Booolean] by_vowels Use vowels as boundries for particles if true
  # @return [Array] particles generated from word
  #
  def self.from_word(word, by_vowels=false)
    if by_vowels
      WordMix::divide_by_vowels(word)
    else
      WordMix::divide(word)
    end
  end

  ##
  # Mix some or all particles in a word
  # @param [String,Array] word String or Array of Particles to mix
  # @param [Hash] options Option parameter hash
  # @option options [Boolean] :random Randomly select particles to alter
  # @option options [Number] :limit Maximum number of particles to alter if random
  # @option options [Symbol] :mode Mix mode; either :shuffle or :flip 
  #   (:shuffle by default)
  # @return [Array] Mixed particles
  def self.mix(word, options={})

    random = options.fetch(:random, true)
    limit = options.fetch(:limit, 65535)
    mode = options.fetch(:mode, :shuffle)

    word = WordMix::divide_by_vowels if !word.is_a?(Array)

    lsel = word.map{|l| 1}.fill{|i| i}

    # Pick indexes to modify randomly if random mode
    lsel = lsel.shuffle![0..limit] if random

    lsel.each do |s|
      if mode == :shuffle
        word[s] = WordMix::divide(word[s]).shuffle!.join("")
      elsif mode == :flip
        word[s] = word[s].reverse
      end
    end

    return word

  end

end